# A simple container, which we use to replace the outdated deploybot image.

It comes with

- Vanilla Ubuntu 16
- GIT
- NodeJS 7
- PHP 7
- Eslint, SassLint, Gulp, Yarn and Bower
- Composer, PHPUnit and PHPCS/PHPCBF